export interface Message {
  data: string;
  date?: Date;
}
