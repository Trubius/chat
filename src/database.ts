import { Message } from './models';

export class Messages {
  private messages: Message[] = [];

  public getMessages(pageNumber: number) {
    const count = this.messages.length;
    const pageSize = 10;

    if (count === 0) {
      console.info('No messages');
    }

    const items = this.messages.slice((pageNumber - 1) * pageSize, pageNumber * pageSize);

    return { items, count };
  }

  public createMessage(message: Message): Message {
    const newMessage: Message = { ...message, date: new Date() };

    this.messages = [...this.messages, newMessage];

    return newMessage;
  }
}

export const database = new Messages();
