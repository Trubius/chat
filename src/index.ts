import * as Koa from 'koa';
import * as KoaRouter from 'koa-router';
import * as bodyParser from 'koa-bodyparser';
import { database } from './database';
import { Message } from './models';

const app = new Koa();
const router = new KoaRouter();

router.get('/message/:page', (ctx: Koa.Context) => {
  try {
    const { page } = ctx.params;
    const messages = database.getMessages(page);

    if (!messages) {
      ctx.response.status = 404;
      return;
    }
    ctx.response.body = messages;
    ctx.response.status = 200;
  } catch (error) {
    ctx.response.status = 500;
    console.error(error);
  }
});

router.post('/message', (ctx: Koa.Context) => {
  try {
    const body = ctx.request.body;
    const { message }: { message: Message } = body;

    if (!message) {
      ctx.response.status = 400;
      return;
    }
    const newMessage = database.createMessage(message);
    ctx.response.body = newMessage;
    ctx.response.status = 201;
  } catch (error) {
    ctx.response.status = 500;
    console.error(error);
  }
});

app.use(bodyParser());
app.use(router.routes()).use(router.allowedMethods());
app.listen(3000, () => {
  console.log(`Server running on https://localhost:3000`);
});
